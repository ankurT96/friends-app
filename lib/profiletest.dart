import 'package:flutter/material.dart';
import 'package:friends_app/loginpage.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ProfileTest extends StatefulWidget {
  const ProfileTest({Key? key}) : super(key: key);

  @override
  _ProfileTestState createState() => _ProfileTestState();
}

class _ProfileTestState extends State<ProfileTest> {
  SharedPreferences? prefs;
  String? email="";
  String? username="";

  retrieve() async{
    prefs = await SharedPreferences.getInstance();
    email = prefs?.getString("email");
    username = prefs?.getString("username");

    print(username);
    setState(() {

    });
  }


  delete() async
  {
    prefs = await SharedPreferences.getInstance();
    prefs?.remove("isLoggedIn");
    setState(() {

    });
  }

  @override
  void initState() {
    super.initState();
    retrieve();
    // futurePost = fetchPost();
  }

  Widget textfield({@required hintText}) {
    return Material(
      elevation: 4,
      shadowColor: Colors.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
              letterSpacing: 2,
              color: Colors.black54,
              fontWeight: FontWeight.bold,
            ),
            fillColor: Colors.white30,
            filled: true,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide.none)),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   // title: Text(""),
      //
      //   actions: <Widget>[
      //     FlatButton(
      //       textColor: Colors.white,
      //       onPressed: () {
      //
      //
      //
      //       },
      //       child:
      //       Icon(Icons.logout),
      //       // Text("Add",style: TextStyle(fontSize: 20,),),
      //       shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
      //     ),
      //   ],
      //   elevation: 0.0,
      //   // backgroundColor: Color(0xff555555),
      //   backgroundColor: Colors.redAccent,
      //
      //   leading: IconButton(
      //     icon: Icon(
      //       Icons.arrow_back,
      //       color: Colors.white,
      //     ),
      //     onPressed: () {},
      //   ),
      // ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 450,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          textfield(
                            hintText: username,
                          ),
                          SizedBox(height: 20,),
                          textfield(
                            hintText: email,
                          ),
                        ],
                      ),
                    ),



                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        child: Text('Logout',
                            style: TextStyle(
                              fontSize: 18,
                            )),
                        onPressed: () {
                          // delete();
                          // Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                          print('Pressed Logout');
                        },
                      ),
                    ),





                    // textfield(
                    //   hintText: 'Password',
                    // ),
                    // textfield(
                    //   hintText: 'Confirm password',
                    // ),
                    // Container(
                    //   height: 55,
                    //   width: double.infinity,
                    //   child: RaisedButton(
                    //     onPressed: () {},
                    //     color: Colors.black54,
                    //     child: Center(
                    //       child: Text(
                    //         "Update",
                    //         style: TextStyle(
                    //           fontSize: 23,
                    //           color: Colors.white,
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // )
                  ],
                ),


              )
            ],
          ),
          CustomPaint(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderCurvedContainer(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                  "Profile",
                  style: TextStyle(
                    fontSize: 35,
                    letterSpacing: 1.5,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width / 2,
                height: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 5),
                  shape: BoxShape.circle,
                  color: Colors.white,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    // image: AssetImage('images/profile.png'),
                    image: AssetImage('assets/images/myprofile.jpg'),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 270, left: 184),
            child: CircleAvatar(
              backgroundColor: Colors.purple,
              child: IconButton(
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                onPressed: () {
                  delete();
                  // delete();
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}

class HeaderCurvedContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // Paint paint = Paint()..color = Color(0xff555555);
    Paint paint = Paint()..color = Colors.redAccent;
    Path path = Path()
      ..relativeLineTo(0, 150)
      ..quadraticBezierTo(size.width / 2, 225, size.width, 150)
      ..relativeLineTo(0, -150)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
