import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:friends_app/bottomnavigationdemo.dart';
import 'package:friends_app/registerpage.dart';
import 'package:shared_preferences/shared_preferences.dart';


import 'mycustomeui.dart';
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController textEditingControllerEmail = TextEditingController();
  TextEditingController textEditingControllerPassword = TextEditingController();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences? prefs;

  saveLoginData() async
  {
    prefs = await SharedPreferences.getInstance();
    prefs?.setString("email", textEditingControllerEmail.text.toString());
    prefs?.setBool("isLoggedIn", true);
    // prefs?.setString("username", textEditingControllerEmail.text.toString());
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset : false,
      // appBar: AppBar(
      //   title: Text('Login Page'),
      // ),
      // body: ScrollConfiguration(
      //   behavior: MyBehavior(),
      //   child: chi,
      //
      // ),
      body:Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              // Color(0xffFEC37B),
              // Color(0xffFF4184),
              Colors.red,
              Colors.purple,
            ],
          )
        ),
        child: Column(mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Container(
              width: size.width * .9,
              height: size.width * 0.9,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
              ),
              child:Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                  children: [

                    Text(
                      'LogIn Page',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(.7),
                      ),
                    ),
                    // SizedBox(),

                    Container(
                      height: size.width / 7,
                      width: size.width / 1.22,
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(right: size.width / 30),
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(.05),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextFormField(controller: textEditingControllerEmail,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.email_outlined,
                            color: Colors.black.withOpacity(.7),
                          ),
                          labelText: "Email",
                          hintText: "abc@gmail.com",

                        ),
                      ),
                    ),
                    // SizedBox(),
                    Container(
                      height: size.width / 7,
                      width: size.width / 1.22,
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(right: size.width / 30),
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(.05),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextFormField(controller: textEditingControllerPassword,
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.lock_outline,
                            color: Colors.black.withOpacity(.7),
                          ),
                          labelText: "Password",
                        ), ),
                    ),

                    Column(

                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            // width: size.width / width,
                            Container(
                              // height: size.width / 8,
                              //   width: size.width / 2.6,
                              // width: 200,
                                child: Row(
                                  children: [

                                    ButtonTheme(
                                      // minWidth: 200.0,
                                      // height: 200.0,
                                      child: ElevatedButton(onPressed: () {

                                        loginuser();
                                      }, child: Text("LOGIN",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,

                                        ),)),
                                    ),

                                    SizedBox(width: size.width / 10),

                                     RichText(
                                      text: TextSpan(
                                        text: 'Sign In?',
                                        style: TextStyle(color: Colors.blueAccent,fontSize: 20,fontWeight: FontWeight.bold,decoration: TextDecoration.underline),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                              // content: Text("Sign In! button pressed"),
                                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RegisterPage()));
                                            // Fluttertoast.showToast(
                                            //   msg:
                                            //   'Forgotten password! button pressed',
                                            // );
                                          },
                                      ),
                                    ),

                                    // ElevatedButton(onPressed: () {
                                    //   Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => RegisterPage()));
                                    //   // registeruser();
                                    // }, child: Text("Register User?")),

                                    // SizedBox(width: size.width / 25),
                                    //
                                    // ElevatedButton(onPressed: () {
                                    //
                                    //   firebaseAuth.sendPasswordResetEmail(email: textEditingControllerEmail.text);
                                    // }, child: Text("Foreget Password?"))
                                  ],
                                ),


                            ),


                          ],
                        ),
                        // Column(
                        //   children: [
                        //     ElevatedButton(
                        //         onPressed: () {
                        //           firebaseAuth.sendPasswordResetEmail(
                        //               email: textEditingControllerEmail.text);
                        //         },
                        //         child: Text("Foreget Password?"))
                        //   ],
                        // )
                        SizedBox(height: 20,),

                        RichText(
                          text: TextSpan(
                            text: 'Forget Password?',
                            style: TextStyle(color: Colors.blueAccent,fontSize: 18, fontWeight: FontWeight.bold,decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                  firebaseAuth.sendPasswordResetEmail(email: textEditingControllerEmail.text);

                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text("Forgotten password! button pressed"),
                                ));
                                // Fluttertoast.showToast(
                                //   msg:
                                //   'Forgotten password! button pressed',
                                // );
                              },
                          ),
                        ),

                      ],
                    )

                  ],
                ),
              ),
            ),

          ],
        ),
      ) ,
    );
  }

  Future<void> loginuser() async {
    try {
      var loginuserbyemailcred = await firebaseAuth.signInWithEmailAndPassword(
          email: textEditingControllerEmail.text,
          password: textEditingControllerPassword.text);
      if (loginuserbyemailcred.user != null) {
        print(loginuserbyemailcred.user!.email);
        saveLoginData();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Login Successful")));

        Navigator.pushReplacement(
          context,
          // MaterialPageRoute(builder: (context) => MyCustomUI()),);
          MaterialPageRoute(builder: (context) => BottomNavigationDemo()),);
      }
    } on FirebaseAuthException catch (e) {
      print(e.toString());
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(e.code)));
    }
  }

  Future<void> registeruser() async {
    try {
      var registeruserbyemailcred = await firebaseAuth.createUserWithEmailAndPassword(
          email: textEditingControllerEmail.text,
          password: textEditingControllerPassword.text);
      if (registeruserbyemailcred.user != null) {
        print(registeruserbyemailcred.user!.email);

      }
    } on FirebaseAuthException catch (e) {
      print(e.toString());
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(e.code)));
    }
  }

}

