import 'package:flutter/material.dart';
import 'package:friends_app/moviemodel.dart';

class CharacterDescriptionUi extends StatefulWidget {
  // const CharacterDescriptionUi({Key? key}) : super(key: key);
  MovieModel? movieModel;

  CharacterDescriptionUi(this.movieModel);

  @override
  _CharacterDescriptionUiState createState() => _CharacterDescriptionUiState();
}

class _CharacterDescriptionUiState extends State<CharacterDescriptionUi> {

  Future<bool> _onBackPressed() async {
    bool goBack = false;
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          // content: Text('Exit From The App'),
          content: Text('Do you want to go back?'),
          actions: [
            TextButton(
              onPressed: () {
                goBack = false;
                Navigator.pop(context);
              },
              child: Text('No'),
            ),
            TextButton(
              onPressed: () {
                goBack = true;
                Navigator.pop(context);
              },
              child: Text('Yes'),
            ),
          ],
        );
      },
    );
    return goBack;
  }

  @override
  Widget build(BuildContext context) {
    // return Scaffold(
    // body:
    // SingleChildScrollView(
    //   child: Column(
    //     children: [
    //       Image.network(widget.movieModel!.image!),
    //       Text(widget.movieModel!.name!,style: TextStyle(
    //           fontSize: 25,
    //           fontWeight: FontWeight.bold
    //       )),
    //       SizedBox(height: 10,),
    //       Text(widget.movieModel!.name!,style: TextStyle(backgroundColor: Colors.lightGreenAccent,
    //         fontSize: 15,
    //       )),
    //     ],
    //   ),
    // ),
    // );


    return new WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(title: Text("Character Details"),backgroundColor: Colors.purple,),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                children: [
                  // Image.network(widget.movieModel!.image!,width: 300,height: 300,),
                  widget.movieModel!.image == "" ?  Image(image: AssetImage('assets/images/myprofile.jpg')) : Image.network(widget.movieModel!.image!,width: 300,height: 300,),
                  Text(widget.movieModel!.name!,
                      style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.movieModel!.house!,
                        style: TextStyle(
                          backgroundColor: Colors.purple,
                          color: Colors.white,
                          fontSize: 15,
                        )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
