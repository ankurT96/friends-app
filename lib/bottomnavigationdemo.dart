import 'package:flutter/material.dart';
import 'package:friends_app/characterlistscreen.dart';
import 'package:friends_app/dashboardpage.dart';
import 'package:friends_app/friendspage.dart';
import 'package:friends_app/profiledemo.dart';
import 'package:friends_app/profiletest.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';

class BottomNavigationDemo extends StatefulWidget {
  const BottomNavigationDemo({Key? key}) : super(key: key);

  @override
  _BottomNavigationDemoState createState() => _BottomNavigationDemoState();
}

class _BottomNavigationDemoState extends State<BottomNavigationDemo> {
  int _selectedIndex = 0;
  int _selectedIndexForTabBar = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    // Text('Home Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    // CharacterListScreen(),
    DashBoardPage(),
    FriendsPage(),
    // ProfilePage(),
    // ProfileDemo(),
    ProfileTest(),

    // Text('Search Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    // Text('Profile Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  //2
  static List<Widget> _listOfIconsForTabBar = <Widget>[
    Icon(Icons.directions_boat),
    Icon(Icons.directions_bus),
    Icon(Icons.directions_railway),
  ];


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _selectedIndexForTabBar = 0;
    });
  }

  void _onItemTappedForTabBar(int index) {
    setState(() {
      _selectedIndexForTabBar = index+1;
      _selectedIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {

    //5
    final tabBar = new TabBar(labelColor: Colors.white,
      onTap: _onItemTappedForTabBar,
      tabs: <Widget>[
        new Tab(
          text: "BOAT",
        ),
        new Tab(
          text: "BUS",
        ),
        new Tab(
          text: "RAILWAY",
        ),
      ],
    );

    return new DefaultTabController(length: 3,


      // decoration: BoxDecoration(
      //     gradient: LinearGradient(
      //       begin: Alignment.topLeft,
      //       end: Alignment.bottomRight,
      //       colors: [
      //         Color(0xffFEC37B),
      //         Color(0xffFF4184),
      //       ],
      //     )
      // ),
      child: new Scaffold(

        // appBar:
        // _selectedIndex == 0 ?
        // AppBar(bottom: tabBar,backgroundColor: Colors.purple, title: const Text('Tabs Demo')):
        //   AppBar(
        //     title: const Text('Flutter BottomNavigationBar Example'),
        //     backgroundColor: Colors.purple,
        // ),
        // appBar: AppBar(
        //     title: const Text('Flutter BottomNavigationBar Example'),
        //     backgroundColor: Colors.purple,
        // ),
        body: Center(
            child:
            _selectedIndexForTabBar == 0 ?
           _widgetOptions.elementAt(_selectedIndex):
            _listOfIconsForTabBar.elementAt(_selectedIndexForTabBar - 1 )
          // child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: GradientBottomNavigationBar(
    backgroundColorStart: Colors.purple,
    backgroundColorEnd: Colors.red,

            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home),

                  title: Text('Home'),
                // backgroundColor: Colors.transparent,
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.people),
                  title: Text('Friends'),
                  // backgroundColor: Colors.yellow
                // backgroundColor: Colors.transparent,
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                title: Text('Profile'),
                // backgroundColor: Colors.blue,
                backgroundColor: Colors.transparent,
              ),
            ],
            type: BottomNavigationBarType.fixed,
            currentIndex: _selectedIndex,
            // selectedItemColor: Colors.black,
            iconSize: 30,
            onTap: _onItemTapped,
            fixedColor: Colors.white,

            // elevation: 5
        ),
      ),
    );
  }
}
