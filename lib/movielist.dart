import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:friends_app/moviemodel.dart';
import 'package:http/http.dart' as http;

import 'characterdescriptionui.dart';

class MovieList extends StatefulWidget {
  const MovieList({Key? key}) : super(key: key);

  @override
  _MovieListState createState() => _MovieListState();
}

Future<List<MovieModel>> fetchPost() async {
  final response =
  await http.get(Uri.parse('http://hp-api.herokuapp.com/api/characters/students'));
  // await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));


  if (response.statusCode == 200) {
    final parsed = json.decode(response.body).cast<Map<String, dynamic>>();

    return parsed.map<MovieModel>((json) => MovieModel.fromMap(json)).toList();
  } else {
    throw Exception('Failed to load Characters');
  }
}


class _MovieListState extends State<MovieList> {
  late Future<List<MovieModel>> futurePost;

  @override
  void initState() {
    super.initState();
    futurePost = fetchPost();
  }

  @override
  Widget build(BuildContext context) {
    return
      // MaterialApp(
      // debugShowCheckedModeBanner: false,
      // title: 'Fetch Data Examples',
      // theme: ThemeData(
      //   primaryColor: Colors.lightBlueAccent,
      // ),
      // home:
      Scaffold(
        // appBar: AppBar(
        //   title: Text('Fetch Data Examples'),
        // ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text("Character List",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w700,),),
            ),
             Expanded(
               child: FutureBuilder<List<MovieModel>>(
                future: futurePost,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (_, index) => Container(
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            // color: Color(0xff97FFFF),
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(10.0),
                          ),


                          child: ListTile(
                              title: Text(snapshot.data![index].name!,style: TextStyle(color: Colors.black),),
                              // leading: Image.network(
                              //   // snapshot.data.[index].image == null
                              //     snapshot.data![index].image!
                              // ),
                              leading: snapshot.data![index].image == "" ?  Image(image: AssetImage('assets/images/myprofile.jpg')) : Image.network(
                                  snapshot.data![index].image!
                              ) ,
                              // subtitle: Text(snapshot.data![index].originalTitle!),
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(
                                  builder: (context) {
                                    return CharacterDescriptionUi(snapshot.data![index]);
                                  },
                                ));
                              }
                          ),

                          // child: Column(
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: [
                          //     Text(
                          //       // "${snapshot.data![index].title}",
                          //       "${snapshot.data![index].name}",
                          //       style: TextStyle(
                          //         fontSize: 18.0,
                          //         fontWeight: FontWeight.bold,
                          //       ),
                          //     ),
                          //     SizedBox(height: 10),
                          //     // Text("${snapshot.data![index].body}"),
                          //   ],
                          // ),
                        ),
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
            ),
             ),
          ],

        ),
      // ),
    );
  }
}
