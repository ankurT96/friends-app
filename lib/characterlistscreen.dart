import 'package:flutter/material.dart';
import 'package:friends_app/characterModel.dart';
import 'package:friends_app/characterbloc.dart';
class CharacterListScreen extends StatefulWidget {
  const CharacterListScreen({Key? key}) : super(key: key);

  @override
  _CharacterListScreenState createState() => _CharacterListScreenState();
}

class _CharacterListScreenState extends State<CharacterListScreen> {
  CharacterBloc characterBloc= CharacterBloc();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("******");
    characterBloc.characterData();
    print("characterData: ${characterBloc.characterData()}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext context,int index){
            return ListTile(
                leading: Icon(Icons.list),
                trailing: Text("GFG",
                  style: TextStyle(
                      color: Colors.green,fontSize: 15),),
                title:Text("List item $index")
            );
          }
      ),
    );

        // Column(
      //   children: [
      //     Expanded(child:
      //     StreamBuilder<CharacterModel>(
      //       stream: characterBloc.characterStreamDisplay,
      //       builder: (context, snapshot) {
      // if (snapshot.hasData) {
      // return ListView.builder(
      //     itemCount: snapshot.data!.length,
      //
      //     itemBuilder: (context, index) =>
      //     ListTile(
      //       title: Text(snapshot.data!.name[index]),
      //     )
      // );
      // }
      //       },
      //
      //     ))
      //   ],
      // ),
    // );
  }
}
