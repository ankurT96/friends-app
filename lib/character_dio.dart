import 'package:dio/dio.dart';
import 'package:friends_app/characterModel.dart';


class CharacterDio{
  Future<CharacterModel?> fetchCharacterFromAPI() async{
    Response response;
    var dio= Dio();
    try{
      response =await dio.get('http://hp-api.herokuapp.com/api/characters/students');
      if(response.statusCode == 200){
        return CharacterModel.fromJson(response.data);
          // CharacterModel.from
      }

    }catch(ex){
      print(ex.toString());

    }

  }
}