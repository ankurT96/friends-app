import 'package:flutter/material.dart';
import 'package:friends_app/characterlistscreen.dart';
import 'package:friends_app/currencyconverter.dart';
import 'package:friends_app/movielist.dart';
class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  int _selectedIndexForTabBar = 0;

  //2
  // static List<Widget> _listOfIconsForTabBar = <Widget>[
  //   Icon(Icons.directions_boat),
  //   Icon(Icons.directions_bus),
  //   Icon(Icons.directions_railway),
  // ];

  // void _onItemTappedForTabBar(int index) {
  //   setState(() {
  //     _selectedIndexForTabBar = index+1;
  //   });
  // }

  @override
  Widget build(BuildContext context) {


    final tabBar = new TabBar(labelColor: Colors.white,
      // onTap: _onItemTappedForTabBar,
      tabs: <Widget>[
        new Tab(
          text: "BOAT",
        ),
        new Tab(
          text: "BUS",
        ),
        // new Tab(
        //   text: "RAILWAY",
        // ),
      ],
    );

    // return Scaffold(
    //   appBar: AppBar(title: Text('Dashboard'),
    //     backgroundColor: Colors.purple,),
    //   body: Center(
    //     child: Text("Dashboard Page"),
    //   ),
    // );

  //   return new DefaultTabController(length: 3, child: new Scaffold(
  //     appBar: AppBar(bottom: tabBar,backgroundColor: Colors.white, title: Text('Tabs Demo')),
  //
  //     //7
  //     body: Center(child:
  //     // _selectedIndexForTabBar == 0 ?
  //     // _listOfIconsForBottomNavigationBar.elementAt(_selectedIndexForBottomNavigationBar):
  //     _listOfIconsForTabBar.elementAt(_selectedIndexForTabBar)),
  //
  //     // bottomNavigationBar: BottomNavigationBar(
  //     //   type: BottomNavigationBarType.fixed,
  //     //   onTap: _onItemTappedForBottomNavigationBar, // this will be set when a new tab is tapped
  //     //   items: [
  //     //     BottomNavigationBarItem(
  //     //         icon: new Icon(Icons.directions_car),title: Text('CAR')),
  //     //     BottomNavigationBarItem(
  //     //         icon: new Icon(Icons.directions_walk),title: Text('WALK')),
  //     //     BottomNavigationBarItem(
  //     //         icon: Icon(Icons.directions_bike),title: Text('BIKE')),
  //     //
  //     //   ],
  //     //   // currentIndex: _selectedIndexForBottomNavigationBar,
  //     // ),
  //   ));
  // }



    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          resizeToAvoidBottomInset : false,

          appBar: AppBar(
            centerTitle: true,
            title: Text('DashBoard',style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
            // flexibleSpace: SafeArea(
              bottom:  TabBar(
                tabs: [
                  // Tab(icon: Icon(Icons.emoji_people), text: "Characters"),
                  // Tab(icon: Icon(Icons.attach_money), text: "Currency Converter")
                  Tab( text: "Characters"),
                  Tab( text: "Currency Converter")
                ],
              ),
            // ),
            // title: Text('Flutter Tabs Demo'),
              backgroundColor: Colors.purple,
            // bottom:
            // // tabBar
            // TabBar(
            //   tabs: [
            //     Tab(icon: Icon(Icons.emoji_people), text: "Characters"),
            //     Tab(icon: Icon(Icons.attach_money), text: "Currency Converter")
            //   ],
            // ),
          ),
          body: TabBarView(
            children: [
              // FirstScreen(),
              // SecondScreen(),
              MovieList(),
              CurrencyConverter(),
              // Center( child: Text("Page 1")),
              // Center( child: Text("Page 2")),
            ],
          ),
        ),
      ),
    );
  }
  }

