import 'package:flutter/material.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';

class Gradient2 extends StatefulWidget {
  const Gradient2({Key? key}) : super(key: key);

  @override
  _Gradient2State createState() => _Gradient2State();
}

class _Gradient2State extends State<Gradient2> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(' Gradiant Background'),
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: Text('Welcome BajarangiSoft'),
      ),
      bottomNavigationBar: GradientBottomNavigationBar(
        backgroundColorStart: Colors.red,
        backgroundColorEnd: Colors.purple,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.message), title: Text('Message')),
          BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Setting')),
          BottomNavigationBarItem(icon: Icon(Icons.mail_outline), title: Text('Mail')),
        ],
        onTap: (index) {
          //Handle button tap
        },
      ),

    );
  }
}
