import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:friends_app/currencymodel.dart';
import 'package:http/http.dart' as http;

class CurrencyConverter extends StatefulWidget {
  const CurrencyConverter({Key? key}) : super(key: key);

  @override
  _CurrencyConverterState createState() => _CurrencyConverterState();
}

String? _chosenValue;
String? _chosenValue2;
String? convertedValue;
TextEditingController inputvalueController =  TextEditingController();
TextEditingController resultAmountController =  TextEditingController();




class _CurrencyConverterState extends State<CurrencyConverter> {


  Future<List<CurrencyModel>> fetchPost() async {
    List<CurrencyModel> list;
    final response = await http.get(Uri.parse(
        'https://v6.exchangerate-api.com/v6/9da12db8c456f85ed67e1e52/latest/$_chosenValue'));

    print("_chosenValue $_chosenValue");

    if (response.statusCode == 200) {

      final jsonResponse = json.decode(response.body);
      Map<String, dynamic> responsedata = jsonResponse;
      convertedValue = responsedata['conversion_rates'][_chosenValue2].toString();
      print(convertedValue);

      print(_chosenValue2);
      print(inputvalueController.text);

      double dConvertedValue= double.parse(convertedValue!);
      double dinputValue= double.parse(inputvalueController.text);
      double dResultAmount= (dConvertedValue * dinputValue);

      setState((){
        resultAmountController.text=dResultAmount.toStringAsFixed(2);
      });

      return list = jsonResponse
          .map<CurrencyModel>((json) => CurrencyModel.fromJson(json))
          .toList();
      // return  CurrencyModel.fromJson(jsonDecode(response.body));
      //     // .toList();
    } else {
      throw Exception('Failed to load Conversion');
    }
  }



  late Future<List<CurrencyModel>> futurePost;

  @override
  void initState() {
    super.initState();
    // futurePost = fetchPost();
    // print(futurePost);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          // color: Colors.black,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            end: Alignment.topLeft,
            begin: Alignment.bottomRight,
            colors: [
              // Color(0xffFEC37B),
              // Color(0xffFF4184),
              Colors.red,
              Colors.purple,
            ],
          )),

          child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Currency Converter",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                      ),
                    ),
                  ),
                  // Text("Select Currency"),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: inputvalueController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelText: "Input Value to convert",
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 18.0,
                              color: Colors.purple)),
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        DropdownButton<String>(
                          focusColor: Colors.white,
                          value: _chosenValue,
                          //elevation: 5,
                          style: TextStyle(color: Colors.white),
                          iconEnabledColor: Colors.black,
                          items: <String>[
                            'AED',
                            'AFN',
                            'USD',
                            'DOP',
                            'GHS',
                            'GIP',
                            'TMT',
                            'INR',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                style: TextStyle(color: Colors.black),
                              ),
                            );
                          }).toList(),
                          hint: Text(
                            // "Please choose a langauage",
                            "Select",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          onChanged: (String? value) {
                            setState(() {
                              _chosenValue = value;
                            });
                          },
                        ),
// SizedBox(width: 10,),

                        ElevatedButton.icon(
                            onPressed: null,
                            icon: Icon(
                              Icons.swap_horiz,
                              size: 40,
                              color: Colors.white,
                            ),
                            label: Text("")),
                        // Elevated.icon(onPressed: null, icon: null, label: null),

                        DropdownButton<String>(
                          focusColor: Colors.white,
                          value: _chosenValue2,
                          //elevation: 5,
                          style: TextStyle(color: Colors.white),
                          iconEnabledColor: Colors.black,
                          items: <String>[
                            'AED',
                            'AFN',
                            'USD',
                            'DOP',
                            'GHS',
                            'GIP',
                            'TMT',
                            'INR',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                style: TextStyle(color: Colors.black),
                              ),
                            );
                          }).toList(),
                          hint: Text(
                            // "Please choose a langauage",
                            "Select2",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          onChanged: (String? value) {
                            setState(() {
                              _chosenValue2 = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Convert',
                          style: TextStyle(
                            fontSize: 18,
                          )),
                      onPressed: () {


                        // print(inputvalueController.text);
                        fetchPost();
                        // setState(() {
                        //   fetchPost();
                        //   // convertedValue;
                        // });
                        print('Pressed');
                      },
                    ),
                  )
                ],
              ),
              // child: Text("Currency Converter"),
            ),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  children: [
                    Text(
                      "Result:",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      // convertedValue!,
                      // '1',
                  resultAmountController.text,
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.lightBlue,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                // child: Text("Currency Converter"),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
