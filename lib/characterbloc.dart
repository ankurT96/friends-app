import 'package:friends_app/characterModel.dart';
import 'package:friends_app/characterrepository.dart';
import 'package:rxdart/rxdart.dart';

class CharacterBloc{
  BehaviorSubject<CharacterModel> _behaviorSubject =
  BehaviorSubject<CharacterModel>();

  ValueStream<CharacterModel> get characterStreamDisplay => _behaviorSubject.stream;
  CharacterRepository characterRepository = CharacterRepository();

  Future<void> characterData() async {
    CharacterModel? characterModel = await characterRepository.fetchCharacterfromRepo();
    if (characterModel != null) {
      // if (movieModel.results != null) {
        _behaviorSubject.sink.add(characterModel);

    } else {
      _behaviorSubject.sink.addError("character Model is null");
    }
  }
}