import 'dart:convert';

List<MovieModel> postFromJson(String str) =>
    List<MovieModel>.from(json.decode(str).map((x) => MovieModel.fromMap(x)));

class MovieModel {
  MovieModel({
    // required this.userId,
    // required this.id,
    required this.name,
    required this.image,
    required this.gender,
    required this.house,
  });

  // int userId;
  // int id;
  String? name;
  String? image;
  String? gender;
  String? house;

  factory MovieModel.fromMap(Map<String, dynamic> json) => MovieModel(
    // userId: json["userId"],
    // id: json["id"],
    name: json["name"],
    image: json["image"],
    gender: json["gender"],
    house: json["house"],
  );
}

// import 'dart:convert';

// List<MovieModel> postFromJson(String str) =>
//     List<MovieModel>.from(json.decode(str).map((x) => MovieModel.fromMap(x)));
//
// class MovieModel {
//   MovieModel({
//     required this.userId,
//     required this.id,
//     required this.title,
//     required this.body,
//   });
//
//   int userId;
//   int id;
//   String title;
//   String body;
//
//   factory MovieModel.fromMap(Map<String, dynamic> json) => MovieModel(
//     userId: json["userId"],
//     id: json["id"],
//     title: json["title"],
//     body: json["body"],
//   );
// }