import 'package:flutter/material.dart';
import 'package:friends_app/mycustomeui.dart';

import 'mydb.dart';

class FriendsPage extends StatefulWidget {
  const FriendsPage({Key? key}) : super(key: key);

  @override
  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  String? myName;

  TextEditingController textEditingControllerfname =TextEditingController();
  TextEditingController textEditingControllerlname =TextEditingController();
  TextEditingController textEditingControllerMobile =TextEditingController();
  TextEditingController textEditingControllerLocation =TextEditingController();

  TextEditingController etextEditingControllerfname =TextEditingController();
  TextEditingController etextEditingControllerlname =TextEditingController();
  TextEditingController etextEditingControllerMobile =TextEditingController();
  TextEditingController etextEditingControllerLocation =TextEditingController();

  List<Map> slist = [];

  MyDb mydb = new MyDb(); //mydb new object from db.dart

  @override
  void initState() {
    mydb.open(); //initilization database
    getdata();

    super.initState();
  }

  getdata(){
    Future.delayed(Duration(milliseconds: 500),() async {
      //use delay min 500 ms, because database takes time to initilize.
      slist = await mydb.db.rawQuery('SELECT * FROM students');

      setState(() { }); //refresh UI after getting data from table.
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Friends App',),
        actions: <Widget>[
        // FlatButton(
        //   textColor: Colors.white,
        //   onPressed: () {
        //
        //
        //
        //   },
        //   child: Text("Add",style: TextStyle(fontSize: 20),),
        //   shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
        // ),
      ],
        backgroundColor: Colors.purple,),
      body:
      SingleChildScrollView(
        child: Container(
          child: slist.length == 0?Text("No Friends Data to show."): //show message if there is no any student
          Column(  //or populate list to Column children if there is student data.
            children: slist.map((stuone){
              return Card(
                child: ListTile(
                  leading: Icon(Icons.people),
                  title: Text(stuone["name"]+ " "+stuone["lname"]),
                  subtitle: Text("Mobile No:" + stuone["mobile_no"].toString() + ", Address: " + stuone["address"]),
                  trailing: Wrap(children: [

                    IconButton(
                        onPressed: (){
                          // print("Name: ${textEditingControllerfname.text}");
                          print("Name: $myName");
                          // etextEditingControllerfname.text=textEditingControllerfname.text;
                          // etextEditingControllerlname.text='';
                          // etextEditingControllerMobile.text='';
                          // etextEditingControllerLocation.text='';

                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  scrollable: true,
                                  title: Text('Friends Data'),
                                  content: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Form(
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            controller: textEditingControllerfname,
                                            decoration: InputDecoration(
                                              labelText: 'First Name',

                                            ),
                                          ),
                                          TextFormField(
                                            controller: textEditingControllerlname,
                                            decoration: InputDecoration(
                                              labelText: 'Last Name',
                                            ),
                                          ),
                                          TextFormField(
                                            controller: textEditingControllerMobile,
                                            decoration: InputDecoration(
                                              labelText: 'Mobile Number',
                                            ),
                                          ),
                                          TextFormField(
                                            controller: textEditingControllerLocation,
                                            decoration: InputDecoration(
                                              labelText: 'Location',
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  actions: [
                                    ElevatedButton(style: ElevatedButton.styleFrom(
                                      primary: Colors.purple,
                                    ),
                                        child: Text("Update"),
                                        onPressed: () {












                                          // mydb.db.rawInsert("INSERT INTO students (name, lname, mobile_no, address) VALUES (?, ?, ?,?);",
                                          //     [textEditingControllerfname.text,textEditingControllerlname.text, textEditingControllerMobile.text, textEditingControllerLocation.text]); //add student from form to database
                                          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("New Friends Data Added")));
                                          //
                                          // Navigator.pop(context);

                                        })
                                  ],
                                );
                              });





                    //   Navigator.push(context, MaterialPageRoute(builder: (BuildContext context){
                    //     return EditStudent(rollno: stuone["roll_no"]);
                    //   })); //navigate to edit page, pass student roll no to edit
                    },
                        icon: Icon(Icons.edit)),


                    // IconButton(onPressed: () async {
                    //   await mydb.db.rawDelete("DELETE FROM students WHERE roll_no = ?", [stuone["roll_no"]]);
                    //   //delete student data with roll no.
                    //   print("Data Deleted");
                    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Student Data Deleted")));
                    //   getdata();
                    // }, icon: Icon(Icons.delete, color:Colors.red))


                  ],),
                ),
              );
            }).toList(),
          ),
        ),
      ),


    // ListView.builder(
      //     itemCount: 5,
      //     itemBuilder: (BuildContext context,int index){
      //       return ListTile(
      //           leading: Icon(Icons.list),
      //           // trailing: Text("GFG",
      //           //   style: TextStyle(
      //           //       color: Colors.green,fontSize: 15),),
      //           title:Text("List item $index")
      //       );
      //     }
      // ),
      // Center(
      //   child: Text('Friends Page'),
      // ),

      floatingActionButton:FloatingActionButton(
        backgroundColor: Colors.redAccent,
        onPressed: () {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  scrollable: true,
                  title: Text('Friends Data'),
                  content: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      child: Column(
                        children: [
                          TextFormField(
                            controller: textEditingControllerfname,
                            decoration: InputDecoration(
                              labelText: 'First Name',

                            ),
                          ),
                          TextFormField(
                            controller: textEditingControllerlname,
                            decoration: InputDecoration(
                              labelText: 'Last Name',
                            ),
                          ),
                          TextFormField(
                            controller: textEditingControllerMobile,
                            decoration: InputDecoration(
                              labelText: 'Mobile Number',
                            ),
                          ),
                          TextFormField(
                            controller: textEditingControllerLocation,
                            decoration: InputDecoration(
                              labelText: 'Location',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    ElevatedButton(style: ElevatedButton.styleFrom(
                      primary: Colors.purple,
                    ),
                        child: Text("Submit"),
                        onPressed: () {
                          // Studentmodel studentmodel = Studentmodel();
                          // studentmodel.fName=textEditingControllerfname.text;
                          // studentmodel.lName=textEditingControllerlname.text;
                          // studentmodel.enrollmentNo=textEditingControllerEnrollNo.text;
                          // studentmodel.coarse=textEditingControllerCoarse.text;
                          //
                          // DateTime now = new DateTime.now();
                          // DateTime date = new DateTime(now.year, now.month, now.day, now.hour,now.minute,now.second);
                          // String dateData=date.toString();
                          // studentmodel.uid=dateData.replaceAll("-", "").replaceAll(":", "").replaceAll(".", "").replaceAll(" ", "");
                          //
                          // print("*** ${studentmodel.uid}");
                          // savedUserDataToDB(studentmodel);
                          //
                          // clearText();
                          // Navigator.pop(context);
                          // displayDataFromDB();


                          // print("Timings:  ${DateTime.now().millisecondsSinceEpoch}");

                           // myName= textEditingControllerfname.text;
                          mydb.db.rawInsert("INSERT INTO students (name, lname, mobile_no, address) VALUES (?, ?, ?,?);",
                              [textEditingControllerfname.text,textEditingControllerlname.text, textEditingControllerMobile.text, textEditingControllerLocation.text]); //add student from form to database
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("New Friends Data Added")));

                          Navigator.pop(context);
                          getdata();

                        })
                  ],
                );
              });
        },
        child:Icon(Icons.add),
        // label: Text(""),
      ),


    );
  }
}
